**Device Profile Selector**
===================

Device Profile Selector is a Unity3D plugin developed to be able to easily switch all kinds of settings based on the device your app is running on. It consists of custom profiles and simple rules based on device specification.

----------


**How to setup new profiles**
---------------------------------------

Setting up new profiles is simple. Just follow the steps:

- Create your custom profile class deriving from BaseProfile (see ExampleProfile.cs)

    	[CreateAssetMenu(menuName = "Device Profile", fileName = "Profile")]
		public class ExampleProfile : BaseProfile
		{
			public int TextureDetails = 1;
			public int ViewDistance = 100;
			...
		}
- Create scriptable objects for your different profiles, e.g. *High*, *Middle* and *Low* using
	- Right Click in Assets View
	- Create
	- Device Profile
- Define *Priority*, *Rules* and *Values* of your profile in the inspector

----------------------------

**Using the profiles**
-------------------------
Finding and accessing the correct profile for the device is done by the *Selector* (see Selector.cs)

    // Instantiate the selector with your profile class
    var selector = new Selector<ExampleProfile>();
	
	// Load your profiles from a given path inside your Resources folder
	selector.Init("Profiles");
	
	// Access the selected profile
	Debug.Log("Active profile: " + selector.ActiveProfile);

---------------

**Create your own providers and evaluation rules for device specifications**
-----------------------------------------------
Custom specifications (e,g, *processing power* or *memory*) and evaluation rules (e.g. *greater* or *lesser*) can be defined as follows in your own code without having to touch the plugin code at all.

	[CompareBy("Positive")]
	public class PositiveEvaluator : IEvaluator
	{
		public bool Evaluate(string deviceSpecValue, string ruleValue)
		{
			return int.Parse(deviceSpecValue) > 0;
		}
	}

	/// Custom specification provider to access to total cpu power based and processing frequency and core count.
	[SpecificationType("TotalCpuPower")]
	public class TotalCpuPowerProvider : IProvider
	{
		public string GetSpecificationValue()
		{
			return (SystemInfo.processorCount * SystemInfo.processorFrequency).ToString();
		}
	}

-------------------

**Example code**
--------------------------------------
Example code can be found in *Assets/Example*