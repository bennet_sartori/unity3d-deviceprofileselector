﻿using System;
using System.Collections.Generic;

namespace DeviceProfileSelector.Utils
{
    public class ReflectionHelper
    {
        public static List<Type> FindTypesWithInterface<TInterface>()
        {
            var interfaceTypes = new List<Type>();
        
            var interfaceType = typeof(TInterface);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (var assembly in assemblies)
            {
                var allTypes = assembly.GetTypes();

                foreach (var type in allTypes)
                {
                    if (interfaceType.IsAssignableFrom(type) && type.IsClass && !type.IsAbstract)
                    {
                        interfaceTypes.Add(type);
                    }
                }
            }

            return interfaceTypes;
        }

        public static TAttribute GetAttribute<TAttribute>(Type type) where TAttribute : Attribute
        {
            return Attribute.GetCustomAttribute(type, typeof(TAttribute)) as TAttribute;
        }
    }
}
