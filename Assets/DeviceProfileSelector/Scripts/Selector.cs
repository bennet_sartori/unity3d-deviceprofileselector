﻿using System;
using System.Collections.Generic;
using DeviceProfileSelector.Evaluation;
using DeviceProfileSelector.Specification;
using DeviceProfileSelector.Utils;
using UnityEngine;

namespace DeviceProfileSelector
{
	public class Selector<TProfile> where TProfile : BaseProfile
	{
		public TProfile ActiveProfile { get; protected set; }

		protected Dictionary<string, IProvider> SpecificationProviders = new Dictionary<string, IProvider>();
		protected Dictionary<string, IEvaluator> CompareExpressionEvalators = new Dictionary<string, IEvaluator>();
		
		/// <summary>
		/// Loads all profiles in the given path and sets the ActiveProfile
		/// </summary>
		/// <param name="profilesPath">The path relative to "Resources" where to load the profiles from</param>
		public virtual void Init(string profilesPath)
		{
			LoadSpecificationProviders();
			LoadCompareExpressionEvaluators();
			
			var profiles = Resources.LoadAll<TProfile>(profilesPath);

			ActiveProfile = FindApplicableProfile(profiles);
		}

		public void LogSystemInfo()
		{
			var info = "System Info (Click for details)\n";
			foreach (var provider in SpecificationProviders)
			{
				info += provider.Key + " " + provider.Value.GetSpecificationValue() + "\n";
			}
			info += "Selected: " + ActiveProfile + "\n";

			if (ActiveProfile)
			{
				info += "Rules:";
				foreach (var rule in ActiveProfile.Rules)
				{
					info += " " + rule;
				}

				Debug.Log(info);
			}
		}

		private void LoadSpecificationProviders()
		{
			var availableProviderTypes = ReflectionHelper.FindTypesWithInterface<IProvider>();
			foreach (var type in availableProviderTypes)
			{
				var idAttribute = ReflectionHelper.GetAttribute<SpecificationType>(type);
				if (idAttribute != null)
				{
					SpecificationProviders.Add(idAttribute.Id, Activator.CreateInstance(type) as IProvider);
				}
			}
		}
		
		private void LoadCompareExpressionEvaluators()
		{
			var availableEvaluatorTypes = ReflectionHelper.FindTypesWithInterface<IEvaluator>();
			foreach (var type in availableEvaluatorTypes)
			{
				var idAttribute = ReflectionHelper.GetAttribute<CompareBy>(type);
				if (idAttribute != null)
				{
					CompareExpressionEvalators.Add(idAttribute.Id, Activator.CreateInstance(type) as IEvaluator);
				}
			}
		}

		private TProfile FindApplicableProfile(IEnumerable<TProfile> profiles)
		{
			var preferredProfile = ScriptableObject.CreateInstance<TProfile>();
			
			foreach (var profile in profiles)
			{
				if (IsProfileApplicable(profile) && profile.Priority > preferredProfile.Priority)
				{
					preferredProfile = profile;
				}
			}

			return preferredProfile;
		}

		private bool IsProfileApplicable(TProfile profile)
		{
			foreach (var rule in profile.Rules)
			{
				if (!IsRuleApplicable(rule))
				{
					return false;
				}
			}

			return true;
		}

		private bool IsRuleApplicable(Rule rule)
		{
			if (!SpecificationProviders.ContainsKey(rule.Spec.Id) || 
			    !CompareExpressionEvalators.ContainsKey(rule.CompareBy.Id))
			{
				return false;
			}

			var provider = SpecificationProviders[rule.Spec.Id];
			var evaluator = CompareExpressionEvalators[rule.CompareBy.Id];

			return evaluator.Evaluate(provider.GetSpecificationValue(), rule.Value);
		}
	}
}