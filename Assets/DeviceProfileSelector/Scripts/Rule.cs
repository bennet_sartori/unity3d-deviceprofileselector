﻿using System;
using DeviceProfileSelector.Evaluation;
using DeviceProfileSelector.Specification;

namespace DeviceProfileSelector
{
	[Serializable]
	public class Rule
	{		
		public SpecificationType Spec;
		public CompareBy CompareBy;
		public string Value;

		public override string ToString()
		{
			return "[" + Spec + " " + CompareBy + " " + Value + "]";
		}
	}
}