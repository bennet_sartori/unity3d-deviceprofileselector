﻿using UnityEngine;

namespace DeviceProfileSelector
{
	public class BaseProfile : ScriptableObject
	{
		// If more than one profile matches then the one with the highest priority is selected
		public int Priority = -1;
		
		// An array of rules which all must evaluate to true
		public Rule[] Rules;
	}
}

