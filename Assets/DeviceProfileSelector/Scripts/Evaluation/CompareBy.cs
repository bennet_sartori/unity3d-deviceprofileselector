﻿using System;

namespace DeviceProfileSelector.Evaluation
{
    [Serializable]
    public class CompareBy : Attribute
    {
        public string Id;

        public CompareBy(string id)
        {
            Id = id;
        }

        public override string ToString()
        {
            return Id;
        }
    }
}

