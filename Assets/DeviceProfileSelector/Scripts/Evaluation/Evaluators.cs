﻿using System;
using System.Text.RegularExpressions;

namespace DeviceProfileSelector.Evaluation
{   
    public interface IEvaluator
    {
        bool Evaluate(string deviceSpecValue, string ruleValue);
    }
    
    [CompareBy("Equals")]
    public class EqualsEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return deviceSpecValue == ruleValue;
        }
    }
    
    [CompareBy("NotEquals")]
    public class NotEqualsEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return deviceSpecValue != ruleValue;
        }
    }
    
    [CompareBy("Lesser")]
    public class LesserEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return string.CompareOrdinal(deviceSpecValue, ruleValue) < 0;
        }
    }
    
    [CompareBy("LesserEquals")]
    public class LesserEqualsEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return string.CompareOrdinal(deviceSpecValue, ruleValue) <= 0;
        }
    }
    
    [CompareBy("Greater")]
    public class GreaterEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return string.CompareOrdinal(deviceSpecValue, ruleValue) > 0;
        }
    }

    [CompareBy("GreaterEquals")]
    public class GreaterEqualsEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return string.CompareOrdinal(deviceSpecValue, ruleValue) >= 0;
        }
    }
    
    [CompareBy("Contains")]
    public class ContainsEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return deviceSpecValue.Contains(ruleValue);
        }
    }
    
    [CompareBy("RegularExpression")]
    public class RegularExpressionEvaluator : IEvaluator
    {
        public bool Evaluate(string deviceSpecValue, string ruleValue)
        {
            return Regex.IsMatch(deviceSpecValue, ruleValue);
        }
    }
}

