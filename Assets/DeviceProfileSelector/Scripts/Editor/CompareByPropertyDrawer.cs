﻿using System.Collections.Generic;
using DeviceProfileSelector.Evaluation;
using DeviceProfileSelector.Utils;
using UnityEditor;
using UnityEngine;

namespace DeviceProfileSelector
{
    [CustomPropertyDrawer(typeof(CompareBy))]
    public class CompareByPropertyDrawer : PropertyDrawer
    {        
        private static readonly List<string> AvailableExpressions = new List<string>();
        
        static CompareByPropertyDrawer()
        {
            var evaluatorTypes = ReflectionHelper.FindTypesWithInterface<IEvaluator>();
            foreach (var evaluatorType in evaluatorTypes)
            {
                var idAttribute = ReflectionHelper.GetAttribute<CompareBy>(evaluatorType);
                if (idAttribute != null)
                {
                    AvailableExpressions.Add(idAttribute.Id);
                }
            }
            
            AvailableExpressions.Sort(string.CompareOrdinal);
        }
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var expressionId = property.FindPropertyRelative("Id");
            
            var current = Mathf.Max(0, AvailableExpressions.IndexOf(expressionId.stringValue));
            var selected = EditorGUI.Popup(position, "CompareBy", current , AvailableExpressions.ToArray());

            expressionId.stringValue = AvailableExpressions[selected];
        }
    }
}

