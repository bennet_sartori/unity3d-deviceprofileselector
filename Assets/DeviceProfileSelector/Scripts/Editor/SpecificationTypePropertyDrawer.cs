﻿using System.Collections.Generic;
using DeviceProfileSelector.Specification;
using DeviceProfileSelector.Utils;
using UnityEditor;
using UnityEngine;

namespace DeviceProfileSelector.Editor
{
    [CustomPropertyDrawer(typeof(SpecificationType))]
    public class SpecificationTypePropertyDrawer : PropertyDrawer
    {
        private static readonly List<string> AvailableSpecifications = new List<string>();
        
        static SpecificationTypePropertyDrawer()
        {
            var providerTypes = ReflectionHelper.FindTypesWithInterface<IProvider>();
            foreach (var providerType in providerTypes)
            {
                var idAttribute = ReflectionHelper.GetAttribute<SpecificationType>(providerType);
                if (idAttribute != null)
                {
                    AvailableSpecifications.Add(idAttribute.Id);
                }
            }
            
            AvailableSpecifications.Sort(string.CompareOrdinal);
        }
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var specId = property.FindPropertyRelative("Id");
            
            var current = Mathf.Max(0, AvailableSpecifications.IndexOf(specId.stringValue));
            var selected = EditorGUI.Popup(position, "Specification", current , AvailableSpecifications.ToArray());

            specId.stringValue = AvailableSpecifications[selected];
        }
    }
}

