﻿using System;
using UnityEngine;

namespace DeviceProfileSelector.Specification
{
    public interface IProvider
    {
        string GetSpecificationValue();
    }
    
    [SpecificationType("CpuFrequency")]
    public class CpuFrequencyProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.processorFrequency.ToString();
        }
    }
    
    [SpecificationType("CpuCoreCount")]
    public class CpuCoreCountProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.processorCount.ToString();
        }
    }
    
    [SpecificationType("GraphicsMemory")]
    public class GraphicsMemoryProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.graphicsMemorySize.ToString();
        }
    }
    
    [SpecificationType("GraphicsDeviceName")]
    public class GraphicsDeviceNameProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.graphicsDeviceName;
        }
    }
    
    [SpecificationType("GraphicsApiType")]
    public class GraphicsApiTypeProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.graphicsDeviceVersion;
        }
    }
    
    [SpecificationType("GraphicsSupportMultiThreading")]
    public class GraphicsSupportMultiThreadingProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.graphicsMultiThreaded.ToString();
        }
    }
    
    [SpecificationType("GraphicsMaxTextureSize")]
    public class GraphicsMaxTextureSizeProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.maxTextureSize.ToString();
        }
    }
    
    [SpecificationType("GraphicsShaderLevel")]
    public class GraphicsShaderLevelProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.graphicsShaderLevel.ToString();
        }
    }
        
    [SpecificationType("DeviceModel")]
    public class DeviceModelProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.deviceModel;
        }
    }
    
    [SpecificationType("DeviceType")]
    public class DeviceTypeProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.deviceType.ToString();
        }
    }
    
    [SpecificationType("OperatingSystem")]
    public class OperatingSystemDetailedProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.operatingSystem;
        }
    }
    
    [SpecificationType("OperatingSystemMemorySize")]
    public class OperatingSystemMemorySizeProvider : IProvider
    {
        public string GetSpecificationValue()
        {
            return SystemInfo.systemMemorySize.ToString();
        }
    }
}