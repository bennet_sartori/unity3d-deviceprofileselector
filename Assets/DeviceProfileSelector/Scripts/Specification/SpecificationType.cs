﻿using System;

namespace DeviceProfileSelector.Specification
{
    [Serializable]
    public class SpecificationType : Attribute
    {
        public string Id;

        public SpecificationType(string id)
        {
            Id = id;
        }

        public override string ToString()
        {
            return Id;
        }
    }
}