﻿using DeviceProfileSelector.Evaluation;
using DeviceProfileSelector.Specification;
using UnityEngine;

namespace DeviceProfileSelector.Example
{
	/// <summary>
	/// Custom rule evaluation. The device value must be positive.
	/// </summary>
	[CompareBy("Positive")]
	public class PositiveEvaluator : IEvaluator
	{
		public bool Evaluate(string deviceSpecValue, string ruleValue)
		{
			return int.Parse(deviceSpecValue) > 0;
		}
	}

	/// <summary>
	/// Custom specification provider to access to total cpu power based and processing frequency and core count.
	/// </summary>
	[SpecificationType("TotalCpuPower")]
	public class TotalCpuPowerProvider : IProvider
	{
		public string GetSpecificationValue()
		{
			return (SystemInfo.processorCount * SystemInfo.processorFrequency).ToString();
		}
	}
}

