﻿using UnityEngine;

namespace DeviceProfileSelector.Example
{
	/// <summary>
	/// Your own profile class with values as you desire and easy creation in the editor
	/// </summary>
	[CreateAssetMenu(menuName = "Device Profile", fileName = "Profile")]
	public class ExampleProfile : BaseProfile
	{
		[Header("Details")]
		public int TextureDetails = 1;
		public int ViewDistance = 100;
		
		/// add more parameters you want to have device specific in here
	}
}

