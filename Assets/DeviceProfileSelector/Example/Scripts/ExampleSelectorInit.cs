﻿using UnityEngine;

namespace DeviceProfileSelector.Example
{
	public class ExampleSelectorInit : MonoBehaviour
	{		
		void Start ()
		{
			// Initialize the selector
			var selector = new Selector<ExampleProfile>();
			
			// Load all ExampleProfiles in Resources/Profiles
			selector.Init("Profiles");
			
			// Log the system specs and the active profile
			selector.LogSystemInfo();
		}
	}
}
